package logger_test

import (
	"fmt"
	"testing"

	"gitlab.com/yankom/golang/logger"
)

var defParams logger.ParamsT = logger.Params

func TestLog(t *testing.T) {
	logger.Log(logger.INFO, "Test Log(l Level, s string)")
	logger.Logf(logger.INFO, "Test %s", "Tracef(l Level, format string, a ...any)")
}

func TestTrace(t *testing.T) {
	logger.Trace("Test Trace(s string)")
	logger.Tracef("Test %s", "Tracef(format string, a ...any)")
}

func TestDebug(t *testing.T) {
	logger.Debug("Test Debug(s string)")
	logger.Debugf("Test %s", "Debugf(format string, a ...any)")
}

func TestInfo(t *testing.T) {
	logger.Info("Test Info(s string)")
	logger.Infof("Test %s", "Infof(format string, a ...any)")
}

func TestWarn(t *testing.T) {
	logger.Warn("Test Warn(s string)")
	logger.Warnf("Test %s", "Warnf(format string, a ...any)")
}

func TestError(t *testing.T) {
	logger.Error("Test Error(s string)")
	logger.ErrorE(fmt.Errorf("Test ErrorE(err error)"))
}

func _TestFatal(t *testing.T) {
	logger.Fatal("Test Fatal(s string)")
}

func _TestFatalf(t *testing.T) {
	logger.Fatalf("Test %v", "Fatal(format string, a ...any)")
}

func _TestFatalIfE(t *testing.T) {
	logger.FatalE(nil)
	logger.FatalE(fmt.Errorf("Test FatalE(err error)"))
}

func TestUTC(t *testing.T) {
	logger.Params.UTC = true
	logger.Info("Test UTC=true")
	logger.Params.UTC = false
	logger.Info("Test UTC=false")
	logger.Params = defParams
}

func TestPrintLevel(t *testing.T) {
	logger.Params.PrintLevel = true
	logger.Info("Test PrintLevel=true")
	logger.Params.PrintLevel = false
	logger.Info("Test PrintLevel=false")
	logger.Params = defParams
}

func TestLevels(t *testing.T) {
	t.Log("logger.Params.Level = logger.TRACE")
	logger.Params.Level = logger.TRACE
	allLevels()
	t.Log("logger.Params.Level = logger.DEBUG")
	logger.Params.Level = logger.DEBUG
	allLevels()
	t.Log("logger.Params.Level = logger.INFO")
	logger.Params.Level = logger.INFO
	allLevels()
	t.Log("logger.Params.Level = logger.WARN")
	logger.Params.Level = logger.WARN
	allLevels()
	t.Log("logger.Params.Level = logger.ERROR")
	logger.Params.Level = logger.ERROR
	allLevels()
	t.Log("logger.Params.Level = logger.FATAL")
	logger.Params.Level = logger.FATAL
	allLevels()
}

func allLevels() {
	logger.Trace("trace message")
	logger.Debug("debug message")
	logger.Info("info message")
	logger.Warn("warn message")
	logger.Error("error message")
}
