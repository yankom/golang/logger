package logger

import (
	"fmt"
	"os"
	"runtime"
	"time"
)

type Level int

const (
	TRACE = 0
	DEBUG = 1
	INFO  = 2
	WARN  = 3
	ERROR = 4
	FATAL = 5
)

var levels = [6]string{"TRACE", "DEBUG", "INFO", "WARN", "ERROR", "FATAL"}

type ParamsT struct {
	Level              Level
	PrintLevel         bool
	TimeFormatLayout   string
	UTC                bool
	PrintErrorLocation bool
}

var Params = ParamsT{
	Level:              TRACE,
	TimeFormatLayout:   "2006-01-02 15:04:05",
	UTC:                true,
	PrintLevel:         true,
	PrintErrorLocation: true,
}

func Trace(s string) {
	Log(TRACE, s)
}

func Tracef(format string, a ...any) {
	Trace(fmt.Sprintf(format, a...))
}

func Debug(s string) {
	Log(DEBUG, s)
}

func Debugf(format string, a ...any) {
	Debug(fmt.Sprintf(format, a...))
}

func Info(s string) {
	Log(INFO, s)
}

func Infof(format string, a ...any) {
	Info(fmt.Sprintf(format, a...))
}

func Warn(s string) {
	Log(WARN, s)
}

func Warnf(format string, a ...any) {
	Warn(fmt.Sprintf(format, a...))
}

func Error(s string) {
	printErrorLocation(ERROR)
	Log(ERROR, s)
}

func Errorf(format string, a ...any) {
	printErrorLocation(ERROR)
	Log(ERROR, fmt.Sprintf(format, a...))
}

func ErrorE(err error) {
	if err != nil {
		printErrorLocation(ERROR)
		Log(ERROR, err.Error())
	}
}

func Fatal(s string) {
	printErrorLocation(FATAL)
	Log(FATAL, s)
	os.Exit(1)
}

func Fatalf(format string, a ...any) {
	printErrorLocation(FATAL)
	Log(FATAL, fmt.Sprintf(format, a...))
	os.Exit(1)
}

func FatalE(err error) {
	if err != nil {
		printErrorLocation(FATAL)
		Log(FATAL, err.Error())
		os.Exit(1)
	}
}

func Log(l Level, s string) {
	if l < Params.Level {
		return
	}

	ts := time.Now()
	if Params.UTC {
		ts = ts.UTC()
	}

	if Params.PrintLevel {
		s = fmt.Sprintf("%s %s", levels[l], s)
	}

	fmt.Printf("%v %s\n", ts.Format(Params.TimeFormatLayout), s)
}

func Logf(l Level, format string, a ...any) {
	Log(l, fmt.Sprintf(format, a...))
}

func printErrorLocation(l Level) {
	if Params.PrintErrorLocation {
		pc := make([]uintptr, 15)
		n := runtime.Callers(3, pc)
		frames := runtime.CallersFrames(pc[:n])
		frame, _ := frames.Next()
		Log(l, fmt.Sprintf("%v:%v", frame.File, frame.Line))
	}
}
